"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var device_service_client_1 = require("device-service-client");
var GreatCircle = require("great-circle");
var halter_common_nestjs_1 = require("halter-common-nestjs");
var halter_common_protobuf_1 = require("halter-common-protobuf");
var moment = require("moment");
var topography_service_client_1 = require("topography-service-client");
var uuid = require("uuid");
var Point = /** @class */ (function () {
    function Point(x, y, datum) {
        this.x = x;
        this.y = y;
        this.datum = datum;
        var minDistance = -1 * (Math.pow(2, 13));
        var maxDistance = (Math.pow(2, 13) - 1);
        if (x < minDistance || x > maxDistance) {
            throw Error("Distance from coordinate to datum has to be between " + minDistance + " and " + maxDistance + " meters. Current 'x' distance: " + x + " meters");
        }
        if (y < minDistance || y > maxDistance) {
            throw Error("Distance from coordinate to datum has to be between " + minDistance + " and " + maxDistance + " meters. Current 'y' distance: " + y + " meters");
        }
    }
    Point.prototype.getAs32BitsInteger = function () {
        var packed = this.datum << 28;
        packed |= (this.x & 0x3FFF) << 14;
        packed |= (this.y & 0x3FFF);
        return packed;
    };
    return Point;
}());
exports.Point = Point;
var ConvertedCoordinates = /** @class */ (function () {
    function ConvertedCoordinates(points, datumLat, datumLong) {
        this.points = points;
        this.datumLat = datumLat;
        this.datumLong = datumLong;
    }
    return ConvertedCoordinates;
}());
exports.ConvertedCoordinates = ConvertedCoordinates;
var farmId = '4ef316f1-4628-4457-9468-1a294d1e7769';
var backendAddress = 'https://halter-core.staging.halter.io/v1';
// TODO get a real secret (talk to Kurt)
var apiSecret = 'some secret';
var authHeaders = { headers: halter_common_nestjs_1.ComponentToComponentUtils.constructAuthHeaders(apiSecret, { farmId: farmId }) };
var serialNumber = process.argv[2]; // getting the first argument
function sendPayload() {
    return __awaiter(this, void 0, void 0, function () {
        var featureApi, findAllFeaturesResponse, datum, minutesToWait, coordinates, boundaryCoordinates, convertedBoundaryCoordinates, convertedCoordinates, setZoneCommand, request, commandRequest, message, singlePayload, frame, packet, messagingApi, scheduleRequestObj;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    featureApi = new topography_service_client_1.FeatureApi(undefined, backendAddress);
                    return [4 /*yield*/, featureApi.findAll(undefined, undefined, authHeaders)];
                case 1:
                    findAllFeaturesResponse = _a.sent();
                    findAllFeaturesResponse.data[0]; // Filter features to get to get the paddock
                    datum = [174.76722624782872, -36.84587168540926];
                    minutesToWait = 3;
                    coordinates = [
                        [
                            [174.777126, -36.862767],
                            [174.777246, -36.862823],
                            [174.777439, -36.862524],
                            [174.777324, -36.862484],
                            [174.777126, -36.862767],
                        ],
                    ];
                    boundaryCoordinates = [
                        [
                            [174.777126, -36.862767],
                            [174.777246, -36.862823],
                            [174.777439, -36.862524],
                            [174.777324, -36.862484],
                            [174.777126, -36.862767],
                        ],
                    ];
                    convertedBoundaryCoordinates = convertCoordinatesToFarmDatumRelativePoints(datum, boundaryCoordinates);
                    convertedCoordinates = convertCoordinatesToFarmDatumRelativePoints(datum, coordinates);
                    setZoneCommand = new halter_common_protobuf_1.SetZoneCommand();
                    setZoneCommand.setTimeUtcSeconds(moment().add(minutesToWait, 'minutes').unix());
                    setZoneCommand.setDatumLat(Math.trunc(convertedCoordinates.datumLat * Math.pow(10, 7)));
                    setZoneCommand.setDatumLong(Math.trunc(convertedCoordinates.datumLong * Math.pow(10, 7)));
                    convertedCoordinates.points.forEach(function (point) { return setZoneCommand.addZone(point.getAs32BitsInteger()); });
                    convertedBoundaryCoordinates.points.forEach(function (point) { return setZoneCommand.addZoneBoundary(point.getAs32BitsInteger()); });
                    request = new halter_common_protobuf_1.Request();
                    commandRequest = new halter_common_protobuf_1.CommandRequest();
                    commandRequest.setType(halter_common_protobuf_1.CommandTypes.SET_ZONE);
                    commandRequest.setId(Math.trunc(Math.random() * 10000));
                    commandRequest.setSetZone(setZoneCommand);
                    request.setBody(setZoneCommand.serializeBinary());
                    request.setType(halter_common_protobuf_1.RequestTypes.COMMAND_REQUEST_TYPE);
                    message = new halter_common_protobuf_1.Message();
                    message.setRequest(request);
                    singlePayload = new halter_common_protobuf_1.SinglePayload();
                    singlePayload.setBody(message.serializeBinary());
                    frame = new halter_common_protobuf_1.Frame();
                    frame.setSinglePayload(singlePayload);
                    packet = new halter_common_protobuf_1.Packet();
                    packet.addFrame(frame);
                    console.log("##### " + new Date().toISOString() + " Payload:", Buffer.from(packet.serializeBinary()).toString('hex'));
                    console.log("##### " + new Date().toISOString() + " Payload (base64):", Buffer.from(packet.serializeBinary()).toString('base64'));
                    messagingApi = new device_service_client_1.MessagingApi({}, '');
                    scheduleRequestObj = {
                        messageToken: uuid(),
                        payloadType: request.getType(),
                        clientName: 'farm-team-script',
                        payload: Buffer.from(packet.serializeBinary()).toString('base64'),
                    };
                    console.log("##### " + new Date().toISOString() + " scheduleRequestObj:", scheduleRequestObj);
                    messagingApi.scheduleRequestMessage(serialNumber, scheduleRequestObj, authHeaders);
                    return [2 /*return*/];
            }
        });
    });
}
function convertCoordinatesToFarmDatumRelativePoints(datum, coordinates) {
    var farmDatumCoordinates = datum;
    var datumId = 0;
    var points = coordinates[0]
        .slice(0, -1) // remove last point to save bytes as first and last point are the same
        .map(function (point) {
        // convert points relative to datum
        var x = GreatCircle.distance(farmDatumCoordinates[1], point[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');
        var y = GreatCircle.distance(point[1], farmDatumCoordinates[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');
        // allow negative placement from datum
        if (point[0] < farmDatumCoordinates[0])
            x *= -1;
        if (point[1] > farmDatumCoordinates[1])
            y *= -1;
        return new Point(x, y, datumId);
    });
    return new ConvertedCoordinates(points, farmDatumCoordinates[1], farmDatumCoordinates[0]);
}
sendPayload();
