import { AxiosResponse } from 'axios';
import { MessagingApi } from 'device-service-client';
import * as GreatCircle from 'great-circle';
import { ComponentToComponentUtils } from 'halter-common-nestjs';
import { ClearStateCommand, CommandRequest, CommandTypes, Frame, Message, Packet, Request, RequestTypes, SetZoneCommand, SinglePayload } from 'halter-common-protobuf';
import * as moment from 'moment';
import { FeatureApi, IFeatureDTO } from 'topography-service-client';
import * as uuid from 'uuid';
import { GeoUtils } from './geo-utils/geo-utils';
import { buildClearStateCommand, buildExitZoneCommand, buildSetZoneCommand } from './geo-utils/zone-builder';



export class Point {
  constructor(readonly x: number, readonly y: number, readonly datum: number) {
    const minDistance = -1 * (Math.pow(2, 13));
    const maxDistance = (Math.pow(2, 13) - 1);
    if (x < minDistance || x > maxDistance) {
      throw Error(`Distance from coordinate to datum has to be between ${minDistance} and ${maxDistance} meters. Current 'x' distance: ${x} meters`);
    }
    if (y < minDistance || y > maxDistance) {
      throw Error(`Distance from coordinate to datum has to be between ${minDistance} and ${maxDistance} meters. Current 'y' distance: ${y} meters`);
    }
  }

  getAs32BitsInteger(): number {
    let packed = this.datum << 28;
    packed |= (this.x & 0x3FFF) << 14;
    packed |= (this.y & 0x3FFF);
    return packed;
  }
}

export class ConvertedCoordinates {
  constructor(readonly points: Point[],
    readonly datumLat: number,
    readonly datumLong: number) { }
}

const farmId = '4ef316f1-4628-4457-9468-1a294d1e7769';
const backendAddress = 'https://halter-core.staging.halter.io/v1';
// TODO get a real secret (talk to Kurt)
const apiSecret = 'some secret';

const authHeaders = { headers: ComponentToComponentUtils.constructAuthHeaders(apiSecret, { farmId }) };
const serialNumber = process.argv[2]; // getting the first argument
const featureName = process.argv[3];



async function sendPayload() {

  // Check for valid serial number and feature name
  if (typeof serialNumber === "undefined" || typeof featureName === "undefined"){
    console.log("Invalid serial number or feature name!");
    return;
  }
  /**
   * Querying topography service to get the polygons
   */

  const featureApi = new FeatureApi(undefined, backendAddress);
  const findAllFeaturesResponse: AxiosResponse<IFeatureDTO[]> = await featureApi.findAll(undefined, undefined, authHeaders);

  // console.log(findAllFeaturesResponse.data[0]);

  for (var i = 0; i < findAllFeaturesResponse.data.length;i++){
    if (findAllFeaturesResponse.data[i].feature.properties.name === featureName){
      var targetFeatureBreak = findAllFeaturesResponse.data[i];
      console.log(targetFeatureBreak);
      break;
    }
  };

  if(typeof targetFeatureBreak === 'undefined'){
    console.log("Break does not exist!");
    return;
  }

  for (var i = 0; i < findAllFeaturesResponse.data.length;i++){
    if (findAllFeaturesResponse.data[i].feature.properties.name === targetFeatureBreak.feature.properties.paddockName){
      var targetFeaturePaddock = findAllFeaturesResponse.data[i];
      console.log(targetFeaturePaddock);
      break;
    }
  };

  if(typeof targetFeaturePaddock === 'undefined'){
    console.log("Cannot find paddock feature for given break!");
    return;
  }
   // Filter features to get to get the paddock


  const datum = [175.589198, -37.687701]; // Farm datum
  // const datum = [174.76722624782872, -36.84587168540926]; // Auckland datum
  // TODO make this a param if you want
  const minutesToWait = 0;

  // TODO Filter features to get polygon for break
  const coordinates: number[][][] = targetFeatureBreak.feature.geometry.coordinates;
  console.log(coordinates);
  // TODO Filter features to get polygon for paddock
  const boundaryCoordinates: number[][][] = targetFeaturePaddock.feature.geometry.coordinates;
  console.log(boundaryCoordinates);

  //let commandRequest = buildSetZoneCommand(targetFeatureBreak.feature.geometry,targetFeaturePaddock.feature.geometry, 0, datum, moment());

  const convertedBoundaryCoordinates = convertCoordinatesToFarmDatumRelativePoints(datum, boundaryCoordinates);
  const convertedCoordinates = convertCoordinatesToFarmDatumRelativePoints(datum, coordinates);
  const setZoneCommand = new SetZoneCommand();
  setZoneCommand.setTimeUtcSeconds(moment().add(minutesToWait, 'minutes').unix());
  setZoneCommand.setDatumLat(Math.trunc(convertedCoordinates.datumLat * Math.pow(10, 7)));
  setZoneCommand.setDatumLong(Math.trunc(convertedCoordinates.datumLong * Math.pow(10, 7)));
  convertedCoordinates.points.forEach(point => setZoneCommand.addZone(point.getAs32BitsInteger()));
  convertedBoundaryCoordinates.points.forEach(point => setZoneCommand.addZoneBoundary(point.getAs32BitsInteger()));
  const request = new Request();
  const commandRequest = new CommandRequest();
  commandRequest.setType(CommandTypes.SET_ZONE);
  commandRequest.setId(Math.trunc(Math.random() * 10000));
  commandRequest.setSetZone(setZoneCommand);
  //
  const messagingApi = new MessagingApi({}, backendAddress);
  const scheduleRequestObj = {
    messageToken: uuid(),
    payloadType: RequestTypes.COMMAND_REQUEST_TYPE,
    clientName: 'farm-team-script',
    payload: Buffer.from(commandRequest.serializeBinary()).toString('base64'),
  };
  console.log(`##### ${new Date().toISOString()} scheduleRequestObj:`, scheduleRequestObj);
  messagingApi.scheduleRequestMessage(serialNumber, scheduleRequestObj, authHeaders);
}

function clearCollarStateRequest(){
  const commandRequest = buildClearStateCommand(0, moment());
  const messagingApi = new MessagingApi({}, backendAddress);
  const scheduleRequestObj = {
    messageToken: uuid(),
    payloadType: RequestTypes.COMMAND_REQUEST_TYPE,
    clientName: 'farm-team-script',
    payload: Buffer.from(commandRequest.serializeBinary()).toString('base64'),
  };
  console.log(`##### ${new Date().toISOString()} scheduleRequestObj:`, scheduleRequestObj);
  messagingApi.scheduleRequestMessage(serialNumber, scheduleRequestObj, authHeaders);
}

async function printFeatures(){

  const featureApi = new FeatureApi(undefined, backendAddress);
  const findAllFeaturesResponse: AxiosResponse<IFeatureDTO[]> = await featureApi.findAll(undefined, undefined, authHeaders);

  for (var i = 0; i < findAllFeaturesResponse.data.length;i++){
    console.log(findAllFeaturesResponse.data[i].feature.properties.name);
  }
}

function convertCoordinatesToFarmDatumRelativePoints(datum, coordinates: number[][][]): ConvertedCoordinates {
  const farmDatumCoordinates = datum;
  const datumId = 0;

  const points = coordinates[0]
      .slice(0, -1) // remove last point to save bytes as first and last point are the same
      .map((point) => {
        // convert points relative to datum
        let x = GreatCircle.distance(farmDatumCoordinates[1], point[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');
        let y = GreatCircle.distance(point[1], farmDatumCoordinates[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');

        // allow negative placement from datum
        if (point[0] < farmDatumCoordinates[0]) x *= -1;
        if (point[1] > farmDatumCoordinates[1]) y *= -1;

        return new Point(x, y, datumId);
      });

  return new ConvertedCoordinates(points, farmDatumCoordinates[1], farmDatumCoordinates[0]);
}


if(featureName === "clear"){
  clearCollarStateRequest();
}else if(serialNumber === "?"){
  printFeatures();
}else{
  sendPayload();
}
