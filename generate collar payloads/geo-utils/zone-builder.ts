import { CommandTypes, CommandRequest, ExitZoneCommand, SetZoneCommand, ClearStateCommand } from 'halter-common-protobuf';
import * as GreatCircle from 'great-circle';
import { Moment } from 'moment';
import { Polygon, Position } from 'geojson';
import { floatDegreesToCollar } from './geo-utils';

export class Point {
  constructor(readonly x: number, readonly y: number, readonly datum: number) {
    const minDistance = -1 * (Math.pow(2, 13));
    const maxDistance = (Math.pow(2, 13) - 1);
    if (x < minDistance || x > maxDistance) {
      throw Error(`Distance from coordinate to datum has to be between ${minDistance} and ${maxDistance} meters. Current 'x' distance: ${x} meters`);
    }
    if (y < minDistance || y > maxDistance) {
      throw Error(`Distance from coordinate to datum has to be between ${minDistance} and ${maxDistance} meters. Current 'y' distance: ${y} meters`);
    }
  }

  getAs32BitsInteger(): number {
    let packed = this.datum << 28;
    packed |= (this.x & 0x3FFF) << 14;
    packed |= (this.y & 0x3FFF);
    return packed;
  }
}

export class ConvertedCoordinates {
  constructor(readonly points: Point[],
              readonly datumLat: number,
              readonly datumLong: number) { }
}

export function buildSetZoneCommand(zone: Polygon, boundary: Polygon, commandId: number, datum: Position, activationTime: Moment): CommandRequest {
  const command = new CommandRequest();
  const nextZoneRequest = new SetZoneCommand();

  const convertedZoneCoordinates = convertCoordinatesToFarmDatumRelativePoints(zone.coordinates[0], datum);
  const convertedBoundaryCoordinates = convertCoordinatesToFarmDatumRelativePoints(boundary.coordinates[0], datum);
  nextZoneRequest.setTimeUtcSeconds(activationTime.unix());
  nextZoneRequest.setDatumLat(floatDegreesToCollar(convertedZoneCoordinates.datumLat));
  nextZoneRequest.setDatumLong(floatDegreesToCollar(convertedZoneCoordinates.datumLong));
  convertedZoneCoordinates.points.forEach(point => nextZoneRequest.addZone(point.getAs32BitsInteger()));
  convertedBoundaryCoordinates.points.forEach(point => nextZoneRequest.addZoneBoundary(point.getAs32BitsInteger()));
  command.setSetZone(nextZoneRequest);
  command.setId(commandId);
  command.setType(CommandTypes.SET_ZONE);
  return command;
}

export function buildClearStateCommand(commandId: number, activationTime: Moment): CommandRequest {
  const command = new CommandRequest();
  const clearStateRequest = new ClearStateCommand();
  clearStateRequest.setTimeUtcSeconds(activationTime.unix());
  command.setClearState(clearStateRequest);
  command.setId(commandId);
  command.setType(CommandTypes.CLEAR_STATE);
  return command;
}

export function buildExitZoneCommand(exitPoint: Position, boundary: Polygon, commandId: number, activationTime: Moment): CommandRequest {
  const command = new CommandRequest();
  const nextZoneRequest = new ExitZoneCommand();
  const datum: Position = boundary.coordinates[0][1];

  const convertedExitPoint = convertCoordinateToFarmDatumRelativePoint(exitPoint, datum);
  const convertedBoundaryCoordinates = convertCoordinatesToFarmDatumRelativePoints(boundary.coordinates[0], datum);
  nextZoneRequest.setTimeUtcSeconds(activationTime.unix());
  nextZoneRequest.setDatumLat(floatDegreesToCollar(datum[1]));
  nextZoneRequest.setDatumLong(floatDegreesToCollar(datum[0]));
  console.log(`converted zone coordinates are ${JSON.stringify(convertedExitPoint)}`);
  nextZoneRequest.setExitTarget(convertedExitPoint.getAs32BitsInteger());
  convertedBoundaryCoordinates.points.forEach(point => nextZoneRequest.addZoneBoundary(point.getAs32BitsInteger()));
  command.setExitZone(nextZoneRequest);
  command.setId(commandId);
  command.setType(CommandTypes.EXIT_ZONE);
  return command;
}

function convertCoordinateToFarmDatumRelativePoint(point: Position, datum: Position) : Point {
  const farmDatumCoordinates = datum;
  let x = GreatCircle.distance(farmDatumCoordinates[1], point[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');
  let y = GreatCircle.distance(point[1], farmDatumCoordinates[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');

  // allow negative placement from datum
  if (point[0] < farmDatumCoordinates[0]) x *= -1;
  if (point[1] > farmDatumCoordinates[1]) y *= -1;
  return new Point(x, y, 0);
}

function convertCoordinatesToFarmDatumRelativePoints(coordinates: Position[], datum: Position): ConvertedCoordinates {
  const farmDatumCoordinates = datum;
  const datumId = 0;

  const points = coordinates
    .slice(0, -1) // remove last point to save bytes as first and last point are the same
    .map((point) => {
      // convert points relative to datum
      let x = GreatCircle.distance(farmDatumCoordinates[1], point[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');
      let y = GreatCircle.distance(point[1], farmDatumCoordinates[0], farmDatumCoordinates[1], farmDatumCoordinates[0], 'M');

      // allow negative placement from datum
      if (point[0] < farmDatumCoordinates[0]) x *= -1;
      if (point[1] > farmDatumCoordinates[1]) y *= -1;

      return new Point(x, y, datumId);
    });

  return new ConvertedCoordinates(points, farmDatumCoordinates[1], farmDatumCoordinates[0]);
}
