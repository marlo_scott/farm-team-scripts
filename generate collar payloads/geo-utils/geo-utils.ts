import * as fs from 'fs';
import { DOMParser } from 'xmldom';
import { FeatureCollection, GeoJSON, LineString, Point, Polygon, Position } from 'geojson';
import * as togeojson from 'togeojson';

export function floatDegreesToCollar(degrees: number): number {
  return Math.trunc(degrees * 1e7);
}

export function collarDegreesToFloat(collar: number): number {
  return collar / 1e7;
}

export class GeoUtils {

  private readonly kmlPath: string;
  private geojson: GeoJSON;

  constructor(kmlPath: string) {
    this.kmlPath = kmlPath;
    this.geojson = GeoUtils.loadKmlToGeoJSON(this.kmlPath);
  }

  private static loadKmlToGeoJSON(path: string): GeoJSON {
    const text = fs.readFileSync(path, 'utf8');
    const kml = new DOMParser().parseFromString(text, 'text/xml');
    return togeojson.kml(kml);
  }

  getPolygon(name: string): Polygon | null {
    if (this.geojson.type === 'FeatureCollection') {
      for (const feature of this.geojson.features) {
        if (feature.properties != null && feature.properties.name != null && feature.properties.name === name) {
          if (feature.geometry.type === 'Polygon') {
            return feature.geometry;
          }
        }
      }
    }
    return null;

  }

  getPoint(name: string): Position | null {
    if (this.geojson.type === 'FeatureCollection') {
      for (const feature of this.geojson.features) {
        if (feature.properties != null && feature.properties.name != null && feature.properties.name === name) {
          if (feature.geometry.type === 'Point') {
            // return null;
            return feature.geometry.coordinates;
          }
        }
      }
    }
    return null;
  }

  getPath(name: string): LineString | null {
    if (this.geojson.type === 'FeatureCollection') {
      for (const feature of this.geojson.features) {
        if (feature.properties != null && feature.properties.name != null && feature.properties.name === name) {
          if (feature.geometry.type === 'LineString') {
            return feature.geometry;
          }
        }
      }
    }
    return null;
  }
}
